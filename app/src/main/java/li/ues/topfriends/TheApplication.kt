package li.ues.topfriends

import android.app.Application
import android.webkit.CookieManager
import androidx.multidex.MultiDexApplication

import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import com.google.firebase.storage.FirebaseStorage
import java.util.concurrent.atomic.AtomicReference
import kotlin.properties.Delegates
import com.google.gson.Gson
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.lang.Exception
import java.util.*


data class ApplicationMeta(
    val latestVersion: Long
)

class Global {
    val cookies: CookieManager = CookieManager.getInstance()
    val storage: FirebaseStorage = FirebaseStorage.getInstance("gs://top-friends-b73c6.appspot.com");
    val check: AtomicReference<Boolean> = AtomicReference()
    var meta = BehaviorSubject.createDefault<ApplicationMeta?>(
        ApplicationMeta(-1)
    )

    init {
        cookies.setAcceptCookie(true);

        storage.getReference("meta.json").getBytes(Long.MAX_VALUE).addOnSuccessListener {
            try {
                val gson = Gson()

                val newMeta = gson.fromJson(String(it, Charsets.UTF_8), ApplicationMeta::class.java)

                meta.onNext(newMeta)
            } catch (ex: Exception) {
                meta.onError(ex)
            }
        }
    }

    companion object {
        val current by lazy {
            Global()
        }
    }
}


/**
 * This is a subclass of [Application] used to provide shared objects for this app, such as
 * the [Tracker].
 */
class TheApplication : MultiDexApplication() {
    private var mTracker: Tracker? = null

    /**
     * Gets the default [Tracker] for this [Application].
     * @return tracker
     */
    // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
    val defaultTracker: Tracker?
        @Synchronized get() {
            if (mTracker == null) {
                val analytics = GoogleAnalytics.getInstance(this)
                mTracker = analytics.newTracker(R.xml.global_tracker)
            }
            return mTracker
        }
}